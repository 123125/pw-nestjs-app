const config = require('config');
const dbConfig = config.get('db');

module.exports = {
    "type": dbConfig.type,
    "host":  process.env.DB_HOST || dbConfig.host,
    "port": Number(process.env.DB_PORT) || dbConfig.port,
    "username": process.env.DB_USERNAME || dbConfig.username,
    "password": process.env.DB_PASSWORD || dbConfig.password,
    "database": process.env.DB_DATABASE || dbConfig.database,
    "migrationsTableName": "custom_migration_table",
    "migrations": ["migration/*{.ts,.js}"],
    "cli": {
        "migrationsDir": "src/migrations"
    }
};