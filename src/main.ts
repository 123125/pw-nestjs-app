import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as config from 'config';
import { AppModule } from './app.module';

function swaggerSetup(app) {
  const swaggerConfig = new DocumentBuilder()
          .setTitle('Parrot Wings')
          .setDescription('REST API Documentation')
          .setVersion('1.0.0')
          .addBearerAuth()
          .build();

  const swaggerDocument = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('/api', app, swaggerDocument);
}

async function bootstrap() {
  const port = config.get('server').port || 3000;
  const origin = config.get('server').origin;
  const logger = new Logger('AppBootstrap');

  const app = await NestFactory.create(AppModule);

  if (process.env.NODE_ENV === 'development') {
    app.enableCors()
  } else {
    app.enableCors({ origin });
    logger.log(`Accepting request from origin ${origin}`)
  }

  swaggerSetup(app);

  await app.listen(port);
  logger.log(`Application is running on port ${port}`);
}
bootstrap();
