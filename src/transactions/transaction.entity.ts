import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { IsDateString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "../user/user.entity";

@Entity()
export class Transaction extends BaseEntity {
    @ApiProperty({example: '1'})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: '3'})
    @Column()
    amount: number;
    
    @ApiProperty({example: '497'})
    @Column()
    balance: number;
    
    @ApiProperty({example: '1'})
    @Column()
    userId: number;

    @ApiProperty({example: 'Test'})
    @Column()
    userName: string;
    
    @ApiProperty({example: '3'})
    @Column()
    recipientId: number;
    
    @ApiProperty({example: 'User1'})
    @Column()
    recipientName: string;

    @ApiProperty({example: '2021-07-08T11:22:16.626Z'})
    @Column()
    @IsDateString()
    createdAt: string = new Date().toISOString();

    @ManyToOne(type => User, user => user.transactions)
    user: User;
} 