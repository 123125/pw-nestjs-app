import { NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.entity';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { GetTransactionsListFilter } from './dto/get-transactions-list-filter.dto';
import { Transaction } from './transaction.entity';
import { TransactionsService } from './transactions.service';

const mockUser = { id: 1, name: 'Test', email: 'test@test.com', save: jest.fn() };
const recipientUser = { id: 3, name: 'User1', email: 'user@test.com', save: jest.fn() };

const mockTransactionRepository = () => ({
  createQueryBuilder: jest.fn(),
  findOne: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
});

const mockUserRepository = () => ({
  findOne: jest.fn(),
})

describe('TransactionsService', () => {
  let service;
  let transactionRepository;
  let userRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        TransactionsService,
        { provide: getRepositoryToken(Transaction), useFactory: mockTransactionRepository },
        { provide: getRepositoryToken(User), useFactory: mockUserRepository }
      ],
    }).compile();

    service = await module.get<TransactionsService>(TransactionsService);
    transactionRepository =  await module.get<Repository<Transaction>>(getRepositoryToken(Transaction));
    userRepository =  await module.get<Repository<User>>(getRepositoryToken(User));
  });

  describe('getTransactions', () => {
    it('gets all transactions from the repository', async () => {

      const createQueryBuilder: any = {
        orderBy: () => createQueryBuilder,
        skip: () => createQueryBuilder,
        where: () => createQueryBuilder,
        take: () => createQueryBuilder,
        getManyAndCount: () => (['someValue', 1]),
      };
    
      jest.spyOn(transactionRepository, 'createQueryBuilder')
        .mockImplementation(() => createQueryBuilder);

      expect(transactionRepository.createQueryBuilder).not.toHaveBeenCalled();
      const filters: GetTransactionsListFilter = { 
        sortField: 'createdAt',
        sortDirection: 'ASC',
        page: 1,
        take: 10
      };
      const result = await service.getTransactions(filters, mockUser);

      expect(transactionRepository.createQueryBuilder).toHaveBeenCalled();

      expect(result.count).toEqual(1);
      expect(result.items).toEqual('someValue');
    });
  });

  describe('getTransactionById', () => {
    it('calls transactionRepository.findOne() and successfully retrieve and return the transaction', async () => {
      transactionRepository.findOne.mockResolvedValue('someValue');

      expect(transactionRepository.findOne).not.toHaveBeenCalled();
      const result = await service.getTransaction(1, mockUser);
      expect(transactionRepository.findOne).toHaveBeenCalled();
      expect(result).toEqual('someValue');

      expect(transactionRepository.findOne).toHaveBeenCalledWith({
        where: {
          id: 1,
          userId: mockUser.id
        }
      });
    });

    it('throws an error if transaction is not found', () => {
      transactionRepository.findOne.mockResolvedValue(null);
      expect(service.getTransaction(1, mockUser)).rejects.toThrow(NotFoundException);
    });
  });

  describe('createTransaction', () => {
    it('calls transactionRepository.create() and returns the result', async () => {
      const save = jest.fn().mockResolvedValue(true);
      transactionRepository.create.mockResolvedValue({
        amount: 4,
        save
      });
      userRepository.findOne.mockResolvedValue(recipientUser);

      expect(transactionRepository.create).not.toHaveBeenCalled();
      expect(userRepository.findOne).not.toHaveBeenCalled();
      expect(mockUser.save).not.toHaveBeenCalled();
      expect(recipientUser.save).not.toHaveBeenCalled();
      expect(save).not.toHaveBeenCalled();
      
      const createTransactionDto: CreateTransactionDto = { amount: 3, recipientId: 3 };
      const result = await service.createTransaction(createTransactionDto, mockUser);

      expect(transactionRepository.create).toHaveBeenCalled();
      expect(userRepository.findOne).toHaveBeenCalled();
      expect(mockUser.save).toHaveBeenCalled();
      expect(recipientUser.save).toHaveBeenCalled();
      expect(save).toHaveBeenCalled();

      expect(result.amount).toEqual(4);
    });
  });

});
