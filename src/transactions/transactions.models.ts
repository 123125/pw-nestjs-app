import { ApiProperty } from "@nestjs/swagger";
import { Transaction } from "./transaction.entity";

export class TransactionListResponse {
    @ApiProperty( {type: [Transaction]})
    items: Transaction[];

    @ApiProperty({example: '1'})
    count: number;
}