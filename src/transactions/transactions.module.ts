import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { AuthModule } from '../auth/auth.module';
import { Transaction } from './transaction.entity';
import { User } from 'src/user/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Transaction, User]),
    AuthModule
  ],
  providers: [TransactionsService],
  controllers: [TransactionsController]
})
export class TransactionsModule {}
