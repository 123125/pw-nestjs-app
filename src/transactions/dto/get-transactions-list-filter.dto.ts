import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNumber, IsOptional, IsPositive, IsString } from "class-validator";

export class GetTransactionsListFilter {
    @ApiProperty({example: '1', required: false})
    @Transform(({ value }) => Number(value), { toClassOnly: true })
    @IsNumber()
    @IsOptional()
    @IsPositive()
    page: number = 1;

    @ApiProperty({example: '10', required: false})
    @Transform(({ value }) => Number(value), { toClassOnly: true })
    @IsNumber()
    @IsOptional()
    take: number = 10;

    @ApiProperty({example: 'createdAt', required: false})
    @IsString()
    @IsOptional()
    sortField: 'id' | 'amount' | 'createdAt' | 'balance' = 'createdAt';
    
    @ApiProperty({example: 'ASC', required: false})
    @IsString()
    @IsOptional()
    @Transform(({ value }) => value.toUpperCase(), { toClassOnly: true })
    sortDirection: 'ASC' | 'DESC' = 'ASC';
}