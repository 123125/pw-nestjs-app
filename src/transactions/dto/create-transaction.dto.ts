import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateTransactionDto {
    @ApiProperty({example: '3'})
    @Transform(({value}) => Math.abs(Number(value)), { toClassOnly: true })
    @IsNumber()
    @IsNotEmpty()
    amount: number;

    @ApiProperty({example: '3'})
    @Transform(({value}) => Number(value), { toClassOnly: true })
    @IsNumber()
    @IsNotEmpty()
    recipientId: number;
}