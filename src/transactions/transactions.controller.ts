import { Body, Controller, Get, Logger, Param, Post, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetUser } from '../decorators/get-user.decorator';
import { User } from '../user/user.entity';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { GetTransactionsListFilter } from './dto/get-transactions-list-filter.dto';
import { Transaction } from './transaction.entity';
import { TransactionListResponse } from './transactions.models';
import { TransactionsService } from './transactions.service';

@ApiTags('Transactions')
@Controller('transactions')
@UseGuards(AuthGuard())
@ApiBearerAuth()
export class TransactionsController {
    private logger = new Logger();

    constructor(private transactionsService: TransactionsService) {}

    @ApiOperation({summary: 'Create Transaction'})
    @ApiResponse({status: 200, type: Transaction})
    @Post()
    createTransaction(
        @Body(new ValidationPipe({transform: true})) dto: CreateTransactionDto,
        @GetUser() user: User
    ): Promise<Transaction> {
        this.logger.verbose(`User "${user.name}" is creating a new transaction. Data: ${JSON.stringify(dto)}`);
        return this.transactionsService.createTransaction(dto, user);
    }

    @ApiOperation({summary: 'Get List of Users'})
    @ApiResponse({status: 200, type: TransactionListResponse})
    @Get()
    getTransactions(
        @Query(new ValidationPipe({transform: true})) dto: GetTransactionsListFilter,
        @GetUser() user: User
    ): Promise<{ items: Transaction[], count: number }> {
        this.logger.verbose(`User "${user.name}" is getting a list of transactions. Filters: ${JSON.stringify(dto)}`);
        return this.transactionsService.getTransactions(dto, user);
    }

    @ApiOperation({summary: 'Get List of Users'})
    @ApiResponse({status: 200, type: Transaction})
    @Get('/:id')
    getTransaction(
        @Param('id') id: string,
        @GetUser() user: User
    ): Promise<Transaction> {
        this.logger.verbose(`User "${user.name}" is getting a list of transactions. Transactions Id: ${id}`);
        return this.transactionsService.getTransaction(+id, user);
    }
}
