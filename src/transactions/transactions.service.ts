import { BadRequestException, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { GetTransactionsListFilter } from './dto/get-transactions-list-filter.dto';
import { Transaction } from './transaction.entity';
import { User } from '../user/user.entity';

@Injectable()
export class TransactionsService {
    private logger = new Logger('TransactionsService');

    constructor(
        @InjectRepository(Transaction)
        private transactionRepository: Repository<Transaction>,

        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {}

    async createTransaction(dto: CreateTransactionDto, user: User): Promise<Transaction> {
        const { recipientId, amount } = dto;
        const recipient = await this.userRepository.findOne(recipientId);

        if (!recipient) {
            throw new NotFoundException(`Recipient with id '${recipientId}' isn't found` );
        }

        if ((user.balance - amount) < 0) {
            throw new BadRequestException()
        }

        user.balance -= amount;
        recipient.balance += amount;

        const transaction = await this.transactionRepository.create({
            amount,
            balance: user.balance,
            user,
            userId: user.id,
            userName: user.name,
            recipientId: recipient.id,
            recipientName: recipient.name
        });

        try {
            await user.save();
            await recipient.save();
            await transaction.save();
            return transaction;
        } catch (error) {
            this.logger.error(`Failed to create transaction for the user '${user.name}' and the recipient '${recipient.name}', Data: ${JSON.stringify(dto)}`, error.stack)
            throw new InternalServerErrorException();
        }
    }

    async getTransactions(dto: GetTransactionsListFilter, user: Partial<User>): Promise<{ items: Transaction[], count: number }> {
        const { page, take, sortField, sortDirection } = dto;
        const skip = (page - 1) * take;

        const query = this.transactionRepository.createQueryBuilder('transaction');
        query.where('transaction.userId = :userId', {userId: user.id});
        query.skip(skip).take(take);
        query.orderBy(`transaction.${sortField}`, sortDirection);

        try {
            const result = await query.getManyAndCount();
            return { items: result[0], count: result[1] }
        } catch (error) {
            this.logger.error(`Failed to get transactions for the user '${user.name}', Filters: ${JSON.stringify(dto)}`, error.stack)
            throw new InternalServerErrorException();
        }
    }

    async getTransaction(id: number, user: User): Promise<Transaction> {
        const transaction = await this.transactionRepository.findOne({ where: {id, userId: user.id} });

        if (!transaction) {
            throw new NotFoundException(`Transactions with id '${id}' isn't found`)
        }

        return transaction;
    }
}
