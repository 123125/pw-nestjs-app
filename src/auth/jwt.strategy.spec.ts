import { UnauthorizedException } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from "../user/user.entity";
import { JwtStrategy } from "./jwt.strategy"

const mockUserRepository = () => ({
    findOne: jest.fn()
})

describe('JwtStrategy', () => {
    let jwtStrategy: JwtStrategy;
    let userRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                JwtStrategy,
                { provide: getRepositoryToken(User), useFactory: mockUserRepository }
            ],
        }).compile();

        jwtStrategy = await module.get<JwtStrategy>(JwtStrategy);
        userRepository = await module.get<Repository<User>>(getRepositoryToken(User));
    });

    describe('validate', () => {
        it('validates and returns the user based on JWT payload', async () => {
            const user = new User();
            user.email = 'test@test.com'
            userRepository.findOne.mockReturnValue(user);

            expect(userRepository.findOne).not.toHaveBeenCalled();
            const result = await jwtStrategy.validate({email: 'test@test.com'});
            expect(userRepository.findOne).toHaveBeenCalled();
            expect(result.email).toEqual(user.email);
        });

        it('throws an unauthorized exception if user cannot be found', async () => {
            userRepository.findOne.mockReturnValue(null);
            const result = jwtStrategy.validate({email: 'test@test.com'});
            expect(result).rejects.toThrow(UnauthorizedException);
        });
    });
})