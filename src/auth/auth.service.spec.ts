import { UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { Test } from "@nestjs/testing";
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from "bcrypt";
import { AuthService } from "./auth.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { LoginUserDto } from "./dto/login-user.dto";
import { User } from '../user/user.entity';

const mockUser = { id: 1, name: 'Test', email: 'test@test.com', save: jest.fn() };

const mockUserRepository = () => ({
    validateUser: jest.fn(),
    findOne: jest.fn(),
    register: jest.fn(),
    create: jest.fn()
});

const mockJwtService = () => ({
    sign: jest.fn()
});

describe('AuthService', () => {
    let service;
    let jwtService;
    let userRepository;
  
    beforeEach(async () => {
      const module = await Test.createTestingModule({
        providers: [
          AuthService,
          { provide: JwtService, useFactory: mockJwtService },
          { provide: getRepositoryToken(User), useFactory: mockUserRepository },
        ],
      }).compile();
  
      service = await module.get<AuthService>(AuthService);
      jwtService = await module.get<JwtService>(JwtService);
      userRepository = await module.get<Repository<User>>(getRepositoryToken(User));
    });

    describe('login', () => {
        it('calls service.validateUser(), jwtService.sign and returns the result', async () => {
            service.validateUser = jest.fn().mockResolvedValue(mockUser);
            jwtService.sign.mockResolvedValue('token');
            expect(service.validateUser).not.toHaveBeenCalled();
            expect(jwtService.sign).not.toHaveBeenCalled();

            const userCredentials: LoginUserDto = { email: 'test@test.com', password: '123456' };
            const result = await service.login(userCredentials);
            expect(service.validateUser).toHaveBeenCalled();
            expect(jwtService.sign).toHaveBeenCalled();
            expect(result.accessToken).toEqual('token');
        });

        it('throws an error if user is not found', async () => {
            userRepository.findOne.mockResolvedValue(null);
            const userCredentials: LoginUserDto = { email: 'test@test.com', password: '123456' };
            const result = service.login(userCredentials);
            expect(result).rejects.toThrow(UnauthorizedException);
        });
    });

    describe('register', () => {
        it('calls userRepository.validateUser(), jwtService.sign and returns the result', async () => {
            userRepository.create.mockResolvedValue(mockUser);
            jwtService.sign.mockResolvedValue('token');

            expect(userRepository.create).not.toHaveBeenCalled();
            expect(jwtService.sign).not.toHaveBeenCalled();

            const userCredentials: CreateUserDto = { name: 'test', email: 'test@test.com', password: '123456' };
            const result = await service.register(userCredentials);

            expect(userRepository.create).toHaveBeenCalled();
            expect(jwtService.sign).toHaveBeenCalled();

            expect(result.accessToken).toEqual('token');
        });
    });

    describe('validateUser', () => {
        let userCredentials: LoginUserDto;

        beforeEach(() => {
            service.validatePassword = jest.fn();
            userCredentials = { email: 'test@test.com', password: '123456' };
        });

        it('returns the user if validation is successful', async () => {
            userRepository.findOne.mockResolvedValue(mockUser);
            service.validatePassword.mockResolvedValue(true);
            const result = await service.validateUser(userCredentials);
            expect(result.email).toEqual('test@test.com')
        });

        it('returns null as user cannot be found', async () => {
            userRepository.findOne.mockResolvedValue(null);
            const result = await service.validateUser(userCredentials);
            expect(service.validatePassword).not.toHaveBeenCalled();
            expect(result).toBeNull();
        });

        it('returns null as password is invalid', async () => {
            userRepository.findOne.mockResolvedValue(mockUser);
            service.validatePassword.mockResolvedValue(false);
            const result = await service.validateUser(userCredentials);
            expect(service.validatePassword).toHaveBeenCalled();
            expect(result).toBeNull();
        });
    });

    describe('validatePassword', () => {
        let user: User;

        beforeEach(() => {
            user = new User();
            user.password = 'somePassword',
            user.salt = 'salt';
            bcrypt.hash = jest.fn();
        });

        it('returns true if password is valid', async () => {
            bcrypt.hash.mockReturnValue('somePassword');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = await service.validatePassword('somePassword', user);
            expect(bcrypt.hash).toHaveBeenCalledWith('somePassword', 'salt');
            expect(result).toEqual(true);
        });

        it('return false if password is invalid', async () => {
            bcrypt.hash.mockReturnValue('wrongPassword');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = await service.validatePassword('wrongPassword', user);
            expect(bcrypt.hash).toHaveBeenCalledWith('wrongPassword', 'salt');
            expect(result).toEqual(false);
        });
    });

});