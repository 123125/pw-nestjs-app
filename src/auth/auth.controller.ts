import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TokenObject } from './auth.models';
import { AuthService } from './auth.service';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {}

    @ApiOperation({summary: 'Login'})
    @ApiResponse({status: 200, type: TokenObject})
    @Post('/login')
    login(@Body(ValidationPipe) dto: LoginUserDto): Promise<TokenObject> {
        return this.authService.login(dto);
    }

    @ApiOperation({summary: 'Register'})
    @ApiResponse({status: 200, type: TokenObject})
    @Post('/register')
    register(@Body(ValidationPipe) dto: CreateUserDto): Promise<TokenObject> {
        return this.authService.register(dto);
    }
}
