import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, Length, Matches } from "class-validator";

export class LoginUserDto {
    @ApiProperty({example: 'test@test.com'})
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty({example: '78ui&*UI'})
    @IsString()
    @Length(4, 20)
    @Matches( /((?=.*\d)|(?!=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    { message: 'Password too weak' })
    password: string;
}