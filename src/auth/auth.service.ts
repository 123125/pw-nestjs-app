import { ConflictException, Injectable, InternalServerErrorException, Logger, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../user/user.entity';
import { DEFAULT_USER_BALANCE, ErrorCode } from '../user/user.models';
import { Repository } from 'typeorm';
import { TokenObject } from './auth.models';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

@Injectable()
export class AuthService {
    private logger = new Logger('AuthService');

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private jwtService: JwtService
    ) {}
    
    async login(dto: LoginUserDto): Promise<TokenObject> {
        const user = await this.validateUser(dto);

        if (!user) {
            throw new UnauthorizedException('Invalid credentials');
        }

        const payload = { email: user.email };
        const accessToken = await this.jwtService.sign(payload);
        this.logger.debug(`Generated JWT Token with payload ${JSON.stringify(payload)}`);
        return { accessToken };
    }

    async register(dto: CreateUserDto): Promise<TokenObject> {
        const { name, email, password } = dto;

        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);

        const user = await this.userRepository.create({
            name,
            email,
            balance: DEFAULT_USER_BALANCE,
            salt,
            password: hashedPassword
        });

        try {
            await user.save();
            const payload = { email: user.email };
            const accessToken = await this.jwtService.sign(payload);
            this.logger.debug(`Generated JWT Token with payload ${JSON.stringify(payload)}`);
            return { accessToken };
        } catch (error) {
            this.logger.error(`Failed to create a new user with the name '${user.name}', Data: ${JSON.stringify(dto)}`, error.stack);
            if (error.code === ErrorCode.DUPLICATE_UNIQUE_FIELD) {
                throw new ConflictException('User with this email has already existed');
            }
            throw new InternalServerErrorException();
        }
    }

    async validateUser(dto: LoginUserDto): Promise<User> {
        const { email, password } = dto;
        const user = await this.userRepository.findOne({ email });
        if (user && await this.validatePassword(password, user)) {
            return user;
        } else {
            return null;
        }
    }

    private async validatePassword(dtoPassword: string, user: User): Promise<boolean> {
        const { password, salt } = user;
        const hash = await bcrypt.hash(dtoPassword, salt);
        return hash === password;
    }
}
