import { ApiProperty } from "@nestjs/swagger";

export class JwtPayload {
    email: string;
}

export class TokenObject {
    @ApiProperty()
    accessToken: string;
}