import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import * as config from "config";
import { InjectRepository } from "@nestjs/typeorm";
import { UnauthorizedException } from "@nestjs/common";
import { Repository } from 'typeorm';
import { JwtPayload } from "./auth.models";
import { User } from '../user/user.entity';

export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: process.env.JWT_SECRET || config.get('jwt').secret,
        })
    }

    async validate(payload: JwtPayload) {
        const { email } = payload;
        const user = await this.userRepository.findOne({ email })

        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}