import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GetUsersListFilter } from './dto/get-users-list-filter.dto';
import { User } from './user.entity';
import { UserListResponse } from './user.models';

@Injectable()
export class UserService {
    private logger = new Logger('UserService');

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {}

    async getUsersList(dto: GetUsersListFilter, user: User): Promise<UserListResponse> {

        let { page, take, search, sortField, sortDirection } = dto;
        const skip = (page - 1) * take;

        const query = this.userRepository.createQueryBuilder('user')
                            .select(['user.id', 'user.name', 'user.email'])
                            .skip(skip || 0)
                            .take(take || 10);

        query.orderBy(`user.${sortField}`, sortDirection);
        query.andWhere('user.email != :email', {email: user.email});

        if (search) {
            query.where('LOWER(user.name) LIKE LOWER(:search)', {search: `%${search}%`});
        }

        try {
            const result = await query.getManyAndCount();
            return { items: result[0], count: result[1] }
        } catch (error) {
            this.logger.error(`Failed to get a list of all users, Filter: ${JSON.stringify(dto)}`, error.stack);
            throw new InternalServerErrorException();
        }
    }

    async getUser(id: number) {
        const foundUser = await this.userRepository.findOne({where: { id } });
        if (!foundUser) {
            throw new NotFoundException(`User with id '${id}' isn't found`)
        }
        return foundUser;
    }

}
