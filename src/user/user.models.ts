import { ApiProperty } from "@nestjs/swagger";

export const DEFAULT_USER_BALANCE: number = 500;

export enum ErrorCode {
    DUPLICATE_UNIQUE_FIELD = '23505'
}

export class UserInfoResponse {
    @ApiProperty({example: '1'})
    id: number;

    @ApiProperty({example: 'test'})
    name: string;

    @ApiProperty({example: 'test@test.com'})
    email: string;

    @ApiProperty({example: '500'})
    balance: number;
}

export class UserResponse {
    @ApiProperty({example: '1'})
    id: number;

    @ApiProperty({example: 'test'})
    name: string;

    @ApiProperty({example: 'test@test.com'})
    email: string;
}

export class UserListResponse {
    @ApiProperty( {type: [UserResponse]})
    items: UserResponse[];

    @ApiProperty({example: '1'})
    count: number;
}