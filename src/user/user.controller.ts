import { Controller, Get, Logger, Param, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetUser } from '../decorators/get-user.decorator';
import { GetUsersListFilter } from './dto/get-users-list-filter.dto';
import { User } from './user.entity';
import { UserInfoResponse, UserListResponse, UserResponse } from './user.models';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
@UseGuards(AuthGuard())
@ApiBearerAuth()
export class UserController {
    private logger = new Logger();

    constructor(private userService: UserService) {}

    @ApiOperation({summary: 'Get User Info'})
    @ApiResponse({status: 200, type: UserInfoResponse})
    @Get('/info')
    getUserInfo(@GetUser() user: User): UserInfoResponse {
        const userInfo = user;
        delete userInfo.password;
        delete userInfo.salt;
        return userInfo;
    }

    @ApiOperation({summary: 'Get List of Users'})
    @ApiResponse({status: 200, type: UserListResponse})
    @Get('/list')
    getUsersList(
        @Query(new ValidationPipe({ transform: true })) dto: GetUsersListFilter,
        @GetUser() user: User
    ): Promise<UserListResponse> {
        this.logger.verbose(`User "${user.name}" is getting a list of the user. Filters: ${JSON.stringify(dto)}`);
        return this.userService.getUsersList(dto, user);
    }

    @ApiOperation({summary: 'Get User By Id'})
    @ApiResponse({status: 200, type: UserResponse})
    @Get('/:id')
    getUser(@Param('id') id: string, @GetUser() user: User): Promise<UserResponse> {
        this.logger.verbose(`User "${user.name}" is getting the user. User Id: ${id}`);
        return this.userService.getUser(+id);
    }
}
