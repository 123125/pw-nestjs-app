import { NotFoundException } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GetUsersListFilter } from "./dto/get-users-list-filter.dto";
import { User } from './user.entity';
import { UserService } from "./user.service";

const mockUser = { id: 1, name: 'Test', email: 'test@test.com' };

const mockUserRepository = () => ({
    createQueryBuilder: jest.fn(),
    findOne: jest.fn(),
})

describe('UserService', () => {
    let service;
    let userRepository;
    let save;
  
    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
            UserService,
            { provide: getRepositoryToken(User), useFactory: mockUserRepository }
            ],
        }).compile();
    
        service = await module.get<UserService>(UserService);
        userRepository = await module.get<Repository<User>>(getRepositoryToken(User));
        save = jest.fn();
    });

    describe('getUsersList', () => {
        it('gets all users from the repository', async () => {
            const createQueryBuilder: any = {
                orderBy: () => createQueryBuilder,
                skip: () => createQueryBuilder,
                where: () => createQueryBuilder,
                take: () => createQueryBuilder,
                select: () => createQueryBuilder,
                andWhere: () => createQueryBuilder,
                getManyAndCount: () => (['someValue', 1]),
              };
            
            jest.spyOn(userRepository, 'createQueryBuilder')
                .mockImplementation(() => createQueryBuilder);

            expect(userRepository.createQueryBuilder).not.toHaveBeenCalled();
            const filters: GetUsersListFilter = { sortField: 'id', sortDirection: 'ASC', page: 1, take: 10, search: 'User' };
            const result = await service.getUsersList(filters, mockUser);
            expect(userRepository.createQueryBuilder).toHaveBeenCalled();
            expect(result.items).toEqual('someValue');
            expect(result.count).toEqual(1);
        });
    });

    describe('getUser', () => {
        it('calls userRepository.findOne() and successfully retrieve and return the user', async () => {
            userRepository.findOne.mockResolvedValue('someValue');
    
            expect(userRepository.findOne).not.toHaveBeenCalled();
            const result = await service.getUser(3);
            expect(userRepository.findOne).toHaveBeenCalledWith({where: {
                id: 3
            }});
            expect(result).toEqual('someValue');
        });

        it('throws an error if user is not found', async () => {
            userRepository.findOne.mockResolvedValue(null);
            const result = service.getUser(3);
            expect(result).rejects.toThrow(NotFoundException);
        });
    });
});